﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace ReflectionHomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            var stopWatch = new Stopwatch();
            const int itr = 100000;

            var testInstance = new MyTestClass().Get();
            var serializer = new Serializer(';');

            var csvStr = string.Empty;
            var resultObj = new MyTestClass();

            // Кастомная сериализация
            stopWatch.Start();
            for(int i = 0; i < itr; i++)
            {
                csvStr = serializer.SerilizeFromObjectToCSV(testInstance);
            }
            stopWatch.Stop();

            Console.WriteLine($"Serialized csv string: {csvStr}");
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine($"RunTime: {elapsedTime}. Iterations: {itr}");

            // Кастомная десериализация
            stopWatch.Reset();
            stopWatch.Start();
            for(int i = 0; i < itr; i++)
            {
                resultObj = serializer.DeserializeFromCSVToObject<MyTestClass>(csvStr);
            }
            stopWatch.Stop();
            Console.WriteLine($"Result object: {resultObj.ToString()}");
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine($"RunTime: {elapsedTime}. Iterations: {itr}");


            // Сериализация Newtonsoft.Json
            Console.WriteLine("Newtonsoft.Json:");
            stopWatch.Reset();
            stopWatch.Start();

            string output = JsonConvert.SerializeObject(testInstance);

            Console.WriteLine($"Serialized csv string: {output}");
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine($"RunTime: {elapsedTime}. Iterations: {itr}");


            // Десериализация Newtonsoft.Json
            stopWatch.Reset();
            stopWatch.Start();

            var deserializedTestClass = JsonConvert.DeserializeObject<MyTestClass>(output);

            Console.WriteLine($"Result object: {deserializedTestClass.ToString()}");
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine($"RunTime: {elapsedTime}. Iterations: {itr}");
        }

    }
}
