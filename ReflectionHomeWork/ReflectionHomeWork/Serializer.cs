﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionHomeWork
{
    public class Serializer
    {
        private readonly char _separator;

        public Serializer(char separator)
        {
            _separator = separator;
        }

        public string SerilizeFromObjectToCSV<T>(T obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            StringBuilder result = new StringBuilder();

            var type = obj.GetType();
            var fields = type.GetFields();
            foreach (var field in fields)
                result.Append(field.Name + ':' + field.GetValue(obj) + _separator);

            var props = type.GetProperties();
            foreach (var prop in props)
                result.Append(prop.Name + ':' + prop.GetValue(obj) + _separator);

            return result.ToString();
        }

        public T DeserializeFromCSVToObject<T>(string csv)
        {
            if (string.IsNullOrEmpty(csv))
                throw new ArgumentNullException(nameof(csv));

            var obj = Activator.CreateInstance<T>();

            foreach(var members in csv.Split(_separator))
            {
                var member = members.Split(':');

                var type = obj.GetType();

                var fields = type.GetFields();
                foreach (var field in fields)
                {
                    if (field.Name.Equals(member[0]))
                        field.SetValue(obj, Convert.ChangeType(member[1], field.FieldType));
                }

                var props = type.GetProperties();
                foreach(var prop in props)
                {
                    if (prop.Name.Equals(member[0]))
                        prop.SetValue(obj, Convert.ChangeType(member[1], prop.PropertyType));
                }


            }

            return obj;
        }
    }
}
