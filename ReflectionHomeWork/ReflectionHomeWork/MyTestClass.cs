﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionHomeWork
{
    public class MyTestClass
    {
        public int Number = 42;

        public string Name { get; set; }

        public MyTestClass()
        {
        }

        public MyTestClass(int number, string name)
        {
            Number = number;
            Name = name;
        }

        public MyTestClass Get()
        {
            return new MyTestClass (1, "Test1");
        }

        public override string ToString()
        {
            return $"Number: {Number}, Name: {Name}";
        }
    }
}
